--
-- PostgreSQL database dump
--

-- Dumped from database version 10.18
-- Dumped by pg_dump version 10.18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: testins(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.testins() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    Declare
        seqar int;
    BEGIN
        select nextval('assureur_id_seq') into seqar;
        insert into assureur(id,designation)values(seqar,new.designation);
        insert into assurance(idvehicule,idassureur,datedebut,datefin,cotisation) values 
        (new.idvehicule,seqar,new.datedebut,new.datefin,new.cotisation);
        return null;
    END;
$$;


ALTER FUNCTION public.testins() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: assurance; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assurance (
    id integer NOT NULL,
    idvehicule integer,
    idassureur integer,
    datedebut timestamp without time zone DEFAULT now(),
    datefin timestamp without time zone,
    cotisation numeric(4,2) NOT NULL
);


ALTER TABLE public.assurance OWNER TO postgres;

--
-- Name: assurance_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.assurance_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assurance_id_seq OWNER TO postgres;

--
-- Name: assurance_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.assurance_id_seq OWNED BY public.assurance.id;


--
-- Name: maxdatevh; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.maxdatevh AS
 SELECT assurance.idvehicule,
    max(assurance.datedebut) AS datedebut
   FROM public.assurance
  GROUP BY assurance.idvehicule;


ALTER TABLE public.maxdatevh OWNER TO postgres;

--
-- Name: assurancerestant; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.assurancerestant AS
 SELECT assurance.id AS idassurance,
    maxdatevh.idvehicule,
    date_part('month'::text, age((assurance.datefin)::timestamp with time zone, now())) AS mois,
    date_part('day'::text, age((assurance.datefin)::timestamp with time zone, now())) AS jour,
    row_number() OVER () AS id
   FROM (public.assurance
     JOIN public.maxdatevh ON (((maxdatevh.idvehicule = assurance.idvehicule) AND (maxdatevh.datedebut = assurance.datedebut))));


ALTER TABLE public.assurancerestant OWNER TO postgres;

--
-- Name: assureur; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assureur (
    id integer NOT NULL,
    idassureur integer,
    designation character varying(20)
);


ALTER TABLE public.assureur OWNER TO postgres;

--
-- Name: assureur_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.assureur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assureur_id_seq OWNER TO postgres;

--
-- Name: assureur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.assureur_id_seq OWNED BY public.assureur.id;


--
-- Name: entretient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entretient (
    id integer NOT NULL,
    idvehicule integer,
    dateentretient timestamp without time zone DEFAULT now(),
    idtypeentretient integer
);


ALTER TABLE public.entretient OWNER TO postgres;

--
-- Name: entretient_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entretient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entretient_id_seq OWNER TO postgres;

--
-- Name: entretient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entretient_id_seq OWNED BY public.entretient.id;


--
-- Name: imagevehicule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.imagevehicule (
    id integer NOT NULL,
    idvehicule integer,
    img text
);


ALTER TABLE public.imagevehicule OWNER TO postgres;

--
-- Name: imagevehicule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.imagevehicule_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.imagevehicule_id_seq OWNER TO postgres;

--
-- Name: imagevehicule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.imagevehicule_id_seq OWNED BY public.imagevehicule.id;


--
-- Name: kilometrage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.kilometrage (
    id integer NOT NULL,
    date date,
    debut double precision NOT NULL,
    fin double precision NOT NULL,
    vehiculeid integer NOT NULL
);


ALTER TABLE public.kilometrage OWNER TO postgres;

--
-- Name: kilometrage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.kilometrage ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.kilometrage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: login; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.login (
    id integer NOT NULL,
    email character varying(255),
    pwd character varying(255)
);


ALTER TABLE public.login OWNER TO postgres;

--
-- Name: login_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.login ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.login_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: produit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.produit (
    id integer NOT NULL,
    nom character varying(255),
    prixunitaire double precision NOT NULL
);


ALTER TABLE public.produit OWNER TO postgres;

--
-- Name: produit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.produit ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.produit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.token (
    id integer NOT NULL,
    dateins timestamp without time zone,
    value character varying(255),
    login_id integer
);


ALTER TABLE public.token OWNER TO postgres;

--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.token ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tokenexp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tokenexp (
    duree time without time zone NOT NULL
);


ALTER TABLE public.tokenexp OWNER TO postgres;

--
-- Name: typeentretient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.typeentretient (
    id integer NOT NULL,
    designation character varying(50)
);


ALTER TABLE public.typeentretient OWNER TO postgres;

--
-- Name: typeentretient_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.typeentretient_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.typeentretient_id_seq OWNER TO postgres;

--
-- Name: typeentretient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.typeentretient_id_seq OWNED BY public.typeentretient.id;


--
-- Name: vassu; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vassu AS
 SELECT a.id,
    a.idvehicule,
    a.idassureur,
    a.datedebut,
    a.datefin,
    a.cotisation,
    ar.designation
   FROM (public.assurance a
     JOIN public.assureur ar ON ((a.idassureur = ar.id)));


ALTER TABLE public.vassu OWNER TO postgres;

--
-- Name: vehicule; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vehicule (
    id integer NOT NULL,
    marque character varying(255)
);


ALTER TABLE public.vehicule OWNER TO postgres;

--
-- Name: vehicule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.vehicule ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.vehicule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: assurance id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assurance ALTER COLUMN id SET DEFAULT nextval('public.assurance_id_seq'::regclass);


--
-- Name: assureur id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assureur ALTER COLUMN id SET DEFAULT nextval('public.assureur_id_seq'::regclass);


--
-- Name: entretient id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entretient ALTER COLUMN id SET DEFAULT nextval('public.entretient_id_seq'::regclass);


--
-- Name: imagevehicule id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.imagevehicule ALTER COLUMN id SET DEFAULT nextval('public.imagevehicule_id_seq'::regclass);


--
-- Name: typeentretient id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.typeentretient ALTER COLUMN id SET DEFAULT nextval('public.typeentretient_id_seq'::regclass);


--
-- Data for Name: assurance; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.assurance (id, idvehicule, idassureur, datedebut, datefin, cotisation) FROM stdin;
1	1	1	2022-12-01 08:00:00	2022-12-31 08:00:00	22.22
2	1	1	2023-01-01 08:00:00	2023-01-31 08:00:00	22.22
3	2	1	2022-12-02 08:00:00	2023-01-02 08:00:00	23.22
4	2	1	2023-01-02 08:00:00	2023-02-28 08:00:00	23.22
5	1	3	2023-02-01 08:00:00	2023-02-28 08:00:00	22.00
\.


--
-- Data for Name: assureur; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.assureur (id, idassureur, designation) FROM stdin;
1	1	MAMA
3	\N	test
\.


--
-- Data for Name: entretient; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.entretient (id, idvehicule, dateentretient, idtypeentretient) FROM stdin;
1	1	2022-12-10 08:00:00	2
2	1	2022-12-12 08:00:00	1
3	1	2022-12-12 08:00:00	3
4	2	2022-12-13 08:00:00	2
5	2	2022-12-13 08:00:00	1
6	2	2022-12-13 08:00:00	3
7	1	2022-12-20 08:00:00	2
8	1	2022-12-20 08:00:00	1
9	1	2022-12-20 08:00:00	3
10	2	2022-12-21 08:00:00	2
11	2	2022-12-21 08:00:00	1
12	2	2022-12-21 08:00:00	3
\.


--
-- Data for Name: imagevehicule; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.imagevehicule (id, idvehicule, img) FROM stdin;
\.


--
-- Data for Name: kilometrage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.kilometrage (id, date, debut, fin, vehiculeid) FROM stdin;
1	2022-12-12	120000	121500	1
2	2022-12-12	200000	201000	2
3	2022-12-13	121500	123000	1
4	2022-12-13	201000	202000	2
5	2022-12-14	123000	124000	1
6	2022-12-14	202000	203000	2
\.


--
-- Data for Name: login; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.login (id, email, pwd) FROM stdin;
1	hasina@gmail.com	root
2	passing@gmail.com	root
3	root@gmail.com	root
4	neks@gmail.com	root
\.


--
-- Data for Name: produit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.produit (id, nom, prixunitaire) FROM stdin;
\.


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.token (id, dateins, value, login_id) FROM stdin;
\.


--
-- Data for Name: tokenexp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tokenexp (duree) FROM stdin;
00:05:00
00:05:00
\.


--
-- Data for Name: typeentretient; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.typeentretient (id, designation) FROM stdin;
1	vidange
2	pneu
3	moteur
\.


--
-- Data for Name: vehicule; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vehicule (id, marque) FROM stdin;
1	Nissan
2	toyota
\.


--
-- Name: assurance_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.assurance_id_seq', 5, true);


--
-- Name: assureur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.assureur_id_seq', 3, true);


--
-- Name: entretient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.entretient_id_seq', 12, true);


--
-- Name: imagevehicule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.imagevehicule_id_seq', 1, false);


--
-- Name: kilometrage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.kilometrage_id_seq', 6, true);


--
-- Name: login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.login_id_seq', 1, false);


--
-- Name: produit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.produit_id_seq', 1, false);


--
-- Name: token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.token_id_seq', 1, false);


--
-- Name: typeentretient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.typeentretient_id_seq', 1, false);


--
-- Name: vehicule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vehicule_id_seq', 2, true);


--
-- Name: assurance assurance_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assurance
    ADD CONSTRAINT assurance_pkey PRIMARY KEY (id);


--
-- Name: assureur assureur_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assureur
    ADD CONSTRAINT assureur_pkey PRIMARY KEY (id);


--
-- Name: entretient entretient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entretient
    ADD CONSTRAINT entretient_pkey PRIMARY KEY (id);


--
-- Name: imagevehicule imagevehicule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.imagevehicule
    ADD CONSTRAINT imagevehicule_pkey PRIMARY KEY (id);


--
-- Name: kilometrage kilometrage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kilometrage
    ADD CONSTRAINT kilometrage_pkey PRIMARY KEY (id);


--
-- Name: login login_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.login
    ADD CONSTRAINT login_pkey PRIMARY KEY (id);


--
-- Name: produit produit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.produit
    ADD CONSTRAINT produit_pkey PRIMARY KEY (id);


--
-- Name: token token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- Name: typeentretient typeentretient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.typeentretient
    ADD CONSTRAINT typeentretient_pkey PRIMARY KEY (id);


--
-- Name: vehicule vehicule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehicule
    ADD CONSTRAINT vehicule_pkey PRIMARY KEY (id);


--
-- Name: vassu trigtestins; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigtestins INSTEAD OF INSERT ON public.vassu FOR EACH ROW EXECUTE PROCEDURE public.testins();


--
-- Name: assurance assurance_idassureur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assurance
    ADD CONSTRAINT assurance_idassureur_fkey FOREIGN KEY (idassureur) REFERENCES public.assureur(id);


--
-- Name: assurance assurance_idvehicule_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assurance
    ADD CONSTRAINT assurance_idvehicule_fkey FOREIGN KEY (idvehicule) REFERENCES public.vehicule(id);


--
-- Name: entretient entretient_idtypeentretient_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entretient
    ADD CONSTRAINT entretient_idtypeentretient_fkey FOREIGN KEY (idtypeentretient) REFERENCES public.typeentretient(id);


--
-- Name: entretient entretient_idvehicule_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entretient
    ADD CONSTRAINT entretient_idvehicule_fkey FOREIGN KEY (idvehicule) REFERENCES public.vehicule(id);


--
-- Name: kilometrage fklua68mslasd5ol2e2dshnbq1m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.kilometrage
    ADD CONSTRAINT fklua68mslasd5ol2e2dshnbq1m FOREIGN KEY (vehiculeid) REFERENCES public.vehicule(id);


--
-- Name: token fkqhk1olyr5s4i5hxfif07yuml6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT fkqhk1olyr5s4i5hxfif07yuml6 FOREIGN KEY (login_id) REFERENCES public.login(id);


--
-- Name: imagevehicule imagevehicule_idvehicule_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.imagevehicule
    ADD CONSTRAINT imagevehicule_idvehicule_fkey FOREIGN KEY (idvehicule) REFERENCES public.vehicule(id);


--
-- PostgreSQL database dump complete
--

